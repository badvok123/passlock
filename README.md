#Pass lock#

A non secure way to display content while disabling input. 
### What this app is not###

* It is not a screen lock
* Not for preventing access
* Not finished :)

### What it is!###

* An input disabler.
* For preventing accidental navigation 

The central principle is that pass lock is designed to solve an incredibly niche problem. One that might not be worth solving. Still, when you pass your device to a friend to display content, video picture etc. often they inadvertently loose the content. 

Pass lock is designed to prevent that.

### How it does this ###

Pass lock is an application that can generate a button that sits on top of every other view. 

This button has a few different features but the main one is creating a 'lock pane' a view that takes up the whole screen but is transparent that sits on top of all other views excluding the button. 

There is also a scale-able view. One in which you can define a rectangular area to remain unlocked while the remaining space will be locked.

### Problems and plans ###

The lock pane serves it's purpose effectively however the i believe a primary cause of loosing content by unfamiliar users is the soft buttons on devices. Mainly the back button. 

Ultimately disabling the back button is very important, more so that the others (home and tab)

A time out kill switch could be useful as-well if the user somehow looses the button that unlocks the phone