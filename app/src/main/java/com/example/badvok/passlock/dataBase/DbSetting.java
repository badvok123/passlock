package com.example.badvok.passlock.dataBase;

import com.example.badvok.passlock.AppDelegate;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by badvok on 18-Oct-15.
 */
public class DbSetting extends RealmObject {

    private String groupname;
    private String name;
    private String data;

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public DbSetting() {

    }

    public DbSetting(String name, String data){
        this.name = name;
        this.data = data;
    }

    public DbSetting(String group, String name, String data) {
        this.groupname = group;
        this.name = name;
        this.data = data;
    }

    public static String getSetting(String name){
        List<DbSetting> settings = AppDelegate.getRealmInstance().where(DbSetting.class).equalTo("name",name).findAll();
        if(settings.size() > 0){
            return settings.get(0).getData();
        }else{
            return "";
        }
    }

  /*  public static void setSetting(String name, int data) {
        setSetting(name, Integer.toString(data));
    }*/

  /*  public static void setSetting(final String name, final String data) {
        AppDelegate.getRealmInstance().executeTransaction(realm1 -> {
            AppDelegate.getRealmInstance()
                    .where(DbSetting.class)
                    .equalTo("name", name)
                    .findAll().clear();

            DbSetting dbSetting = realm1.createObject(DbSetting.class);
            dbSetting.setName(name);
            dbSetting.setData(data);
        });

    }*/



    public static int getDeviceScreenWidth(){
        return 0;
    }

    public static int getDeviceScreenHeight(){
        return 0;
    }
}
