package com.example.badvok.passlock.Controlls.SelectiveLockView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.badvok.passlock.AppDelegate;
import com.example.badvok.passlock.R;

/**
 * Created by badvok on 30-Sep-15.
 */
public class AdjustableView extends FrameLayout {

    View view;
    ImageView expander_top, edge_top, expander_left, edge_left, expander_bottom, edge_bottom, expander_right, edge_right, center;

    int width, height;
    int topHeight = 200, leftWidth = 200, rightWidth = 200, bottomHeight = 200;
    int border = 30;

    public AdjustableView(Context context) {
        super(context);
        init(context);
    }

    public AdjustableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AdjustableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    //TODO add that thing to unread this..lol
    public AdjustableView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context ctx) {
        //TODO add isInEdit mode
        view = inflate(ctx, R.layout.adjustable_view, null);
        addView(view);
        addLayoutViews();
        getScreenDims();
        setViewParams(leftWidth, topHeight, rightWidth, bottomHeight);
        setColours();
        touchListener();
    }

    private void addLayoutViews() {
        expander_top = (ImageView) findViewById(R.id.expander_view_top);
        edge_top = (ImageView) findViewById(R.id.expander_view_edge_top);
        expander_left = (ImageView) findViewById(R.id.expander_view_left);
        edge_left = (ImageView) findViewById(R.id.expander_view_edge_left);
        expander_bottom = (ImageView) findViewById(R.id.expander_view_bottom);
        edge_bottom = (ImageView) findViewById(R.id.expander_view_edge_bottom);
        expander_right = (ImageView) findViewById(R.id.expander_view_right);
        edge_right = (ImageView) findViewById(R.id.expander_view_edge_right);
        center = (ImageView) findViewById(R.id.center);

    }

    private void getScreenDims() {
        width = AppDelegate.getScreenWidth();
        height = AppDelegate.getScreenHeight();

    }
//***************************************************************View sizes*************************************************************
    /**
     * setViewParams is called to refresh the respective views in the adjustable view
     */
    private void setViewParams(int left, int top, int right, int bottom) {
        //Each view needs to be reset when one is changed as multiple values are effected.
        adjustViewParams(expander_top, width, top, Gravity.TOP, 0, 0, 0, 0);
        adjustViewParams(edge_top, width - (left + right + border + border), border, Gravity.TOP, 0, 0, 0, 0);

        adjustViewParams(expander_left, left, height - (top + bottom), Gravity.START, 0, 0, 0, 0);
        adjustViewParams(edge_left, border, height - (top + bottom), Gravity.START, 0, 0, 0, 0);

        adjustViewParams(expander_bottom, width, bottom, Gravity.BOTTOM, 0, 0, 0, 0);
        adjustViewParams(edge_bottom, width - (left + right + border + border), border, Gravity.BOTTOM, 0, 0, 0, 0);

        adjustViewParams(expander_right, right, height - (top + bottom), Gravity.END, 0, 0, 0, 0);
        adjustViewParams(edge_right, border, height - (top + bottom), Gravity.END, 0, 0, 0, 0);

        adjustViewParams(center, width - (left + right + border + border), height - (top + bottom + border + border), Gravity.NO_GRAVITY, 0, 0, 0, 0);

    }

    public void adjustViewParams(View v, int width, int height, int gravity, int left, int top, int right, int bottom) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height, gravity);
        params.setMargins(left, top, right, bottom);
        v.setLayoutParams(params);
    }

    //***************************************************************Save variables*************************************************************
    /**
     * This is called after touch events. Saves the updated variables 'globally'
     */
    private void updateVars(int left, int top, int right, int bottom) {
        topHeight = top;
        leftWidth = left;
        rightWidth = right;
        bottomHeight = bottom;
        saveValues();
    }

    /**
     * The adjustable view is recreated later out of 4 separate views. This saves the values to appDelegate
     */
    private void saveValues() {
        AppDelegate.setAdjustableViewValues(topHeight, leftWidth, rightWidth, bottomHeight);
    }

    //***************************************************************Colours*************************************************************
    /**
     * Colors everything in material awesomeness
     */
    private void setColours() {
        //default colours
        expander_top.setBackgroundColor(getResources().getColor(R.color.material_light_blue));
        expander_left.setBackgroundColor(getResources().getColor(R.color.material_light_blue));
        expander_bottom.setBackgroundColor(getResources().getColor(R.color.material_light_blue));
        expander_right.setBackgroundColor(getResources().getColor(R.color.material_light_blue));
        edge_top.setBackgroundColor(getResources().getColor(R.color.material_dark_purple));
        edge_left.setBackgroundColor(getResources().getColor(R.color.material_dark_purple));
        edge_bottom.setBackgroundColor(getResources().getColor(R.color.material_dark_purple));
        edge_right.setBackgroundColor(getResources().getColor(R.color.material_dark_purple));
    }

    private void setViewColour(View v, int colour) {
        v.setBackgroundColor(colour);
    }

    //***************************************************************On touch listener interface*************************************************************
    private void touchListener() {

        /**
         * Each view, top,bot,left,right has a respective edge for clicking on. top_edge etc.
         * When the user drags an edge it updates the relevant view hights/widths by calling setViewParams
         *
         */
        edge_top.setOnTouchListener(new OnTouchListener() {

            private float initialTouchX, initialTouchY;
            private int heightChange, widthChange;
            private int top = topHeight, bot = bottomHeight, left = leftWidth, right = rightWidth;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setViewColour(edge_top, getResources().getColor(R.color.material_dark_purple_shade2));
                        top = topHeight;
                        bot = bottomHeight;
                        left = leftWidth;
                        right = rightWidth;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        break;
                    case MotionEvent.ACTION_UP:
                        //save updated values
                        updateVars(left, top, right, bot);
                        setViewColour(edge_top, getResources().getColor(R.color.material_dark_purple));
                        break;
                    case MotionEvent.ACTION_MOVE:
                        heightChange = (int) (event.getRawY()) - (int) initialTouchY;
                        top = topHeight + heightChange; //Move is called multiple times during one 'drag' which is why local variable top is used
                        if (top + bot + border + border > height) {
                            //stops views overlapping
                            top = height - bot - border - border - 5;
                        }
                        setViewParams(left, top, right, bot);
                        break;
                }
                return true;
            }
        });
        edge_left.setOnTouchListener(new OnTouchListener() {

            private float initialTouchX, initialTouchY;
            private int heightChange, widthChange;
            private int top = topHeight, bot = bottomHeight, left = leftWidth, right = rightWidth;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //set local height/width vars from the 'global' vars
                        setViewColour(edge_left, getResources().getColor(R.color.material_dark_purple_shade2));
                        top = topHeight;
                        bot = bottomHeight;
                        left = leftWidth;
                        right = rightWidth;
                        //get touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        break;
                    case MotionEvent.ACTION_UP:
                        //save updated values
                        updateVars(left, top, right, bot);
                        setViewColour(edge_left, getResources().getColor(R.color.material_dark_purple));
                        break;
                    case MotionEvent.ACTION_MOVE:
                        //calculate the change since last move.
                        widthChange = (int) (event.getRawX()) - (int) initialTouchX;
                        left = leftWidth + widthChange;
                        if (left + right + border + border > width) {
                            //stops views overlapping
                            left = width - right - border - border - 5;
                        }
                        setViewParams(left, top, right, bot);

                        break;
                }
                return true;
            }
        });
        edge_bottom.setOnTouchListener(new OnTouchListener() {

            private float initialTouchX, initialTouchY;
            private int heightChange, widthChange;
            private int top = topHeight, bot = bottomHeight, left = leftWidth, right = rightWidth;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setViewColour(edge_bottom, getResources().getColor(R.color.material_dark_purple_shade2));
                        top = topHeight;
                        bot = bottomHeight;
                        left = leftWidth;
                        right = rightWidth;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        break;
                    case MotionEvent.ACTION_UP:
                        //save updated values
                        updateVars(left, top, right, bot);
                        setViewColour(edge_bottom, getResources().getColor(R.color.material_dark_purple));
                        break;
                    case MotionEvent.ACTION_MOVE:
                        heightChange = (int) (event.getRawY()) - (int) initialTouchY;
                        bot = bottomHeight - heightChange;
                        if (top + bot + border + border > height) {
                            //stops views overlapping
                            bot = height - top - border - border - 5;
                        }
                        setViewParams(left, top, right, bot);
                        break;
                }
                return true;
            }
        });
        edge_right.setOnTouchListener(new OnTouchListener() {

            private float initialTouchX, initialTouchY;
            private int heightChange, widthChange;
            private int top = topHeight, bot = bottomHeight, left = leftWidth, right = rightWidth;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        setViewColour(edge_right, getResources().getColor(R.color.material_dark_purple_shade2));
                        top = topHeight;
                        bot = bottomHeight;
                        left = leftWidth;
                        right = rightWidth;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        break;
                    case MotionEvent.ACTION_UP:
                        //save updated values
                        updateVars(left, top, right, bot);
                        setViewColour(edge_right, getResources().getColor(R.color.material_dark_purple));
                        break;
                    case MotionEvent.ACTION_MOVE:
                        widthChange = (int) (event.getRawX()) - (int) initialTouchX;
                        right = rightWidth - widthChange;
                        if (right + left + border + border > width) {
                            //stops views overlapping
                            right = width - left - border - border - 5;
                        }
                        setViewParams(left, top, right, bot);

                        break;
                }
                return true;
            }
        });
    }


}
