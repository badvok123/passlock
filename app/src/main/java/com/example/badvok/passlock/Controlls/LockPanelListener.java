package com.example.badvok.passlock.Controlls;

/**
 * Created by badvok on 29-Sep-15.
 */
public interface LockPanelListener {

    void onButtonOnePressed(boolean isLocked);

    void onButtonTwoPressed(boolean isPressed);

    void onButtonThreePressed(boolean isPressed);

}
