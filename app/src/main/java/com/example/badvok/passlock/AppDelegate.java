package com.example.badvok.passlock;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import io.realm.Realm;

/**
 * Created by badvok on 13-Oct-15.
 */
public class AppDelegate extends Application {

    private static int screenHeight = 0;
    private static int screenWidth = 0;

    private static int topHeight = 0;
    private static int bottomHeight = 0;
    private static int leftWidth = 0;
    private static int rightWidth = 0;

    private static int lockButtonXcord = 0;
    private static int lockButtonYcord = 0;



    public static Context applicationContext = null;

    public static Realm getRealmInstance() {
        return Realm.getInstance(applicationContext);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        setScreenDims();

    }

    private static void setScreenDims() {
        //set the dimension of the screen for later use. This method only needs to be called once.
        WindowManager wm = (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;//1080
        screenHeight = size.y;

    }

    public static void setAdjustableViewValues(int top, int left, int right, int bottom) {
        topHeight = top;
        bottomHeight = bottom;
        leftWidth = left;
        rightWidth = right;
    }

    public static void setLockButtonCords(int x, int y){
        lockButtonXcord = x;
        lockButtonYcord = y;
    }

    public static int getLockButtonXcord(){
        return lockButtonXcord;
    }
    public static int getLockButtonYcord(){
        return lockButtonYcord;
    }

    public static int getTopHeight() {
        return topHeight;
    }

    public static int getBottomHeight() {
        return bottomHeight;
    }

    public static int getleftWidth() {
        return leftWidth;
    }

    public static int getRightWidth() {
        return rightWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    /*
    public static void setScreenHeight(int height){
        screenHeight = height;
    }
    public static void setScreenWidth(int width){
        screenWidth = width;
    }
    */

}
