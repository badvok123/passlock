package com.example.badvok.passlock;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.badvok.passlock.Controlls.LockButton;
import com.example.badvok.passlock.Controlls.LockPanelListener;
import com.example.badvok.passlock.Controlls.SelectiveLockView.AdjustableView;

/**
 * Created by badvok on 10-Sep-15.
 * <p/>
 * Here is where views are attached to windowed mode.
 * These views sit on top of EVERYTHING even outside the app.
 */
public class LockService extends Service {

    private WindowManager windowManager;
    private AdjustableView adjustableView;
    private ViewGroup parent;
    private ImageView lockPane, button, topView, leftView, bottomView, rightView;
    private LockButton lockButton;
    private boolean panelIsOpen = false, moving = false;
    int width, height;


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        lockPane = new ImageView(this);
        button = new ImageView(this);
        lockButton = new LockButton(this);
        adjustableView = new AdjustableView(this);

        parent = new FrameLayout(getApplicationContext());

        lockPane.setBackgroundColor(Color.RED);
        button.setImageResource(R.mipmap.ic_lock_open_black_48dp);

        createLockPane();
        createLockButton();

        // setImmersiveFullScreenMode();
        lockButtonPanelListener();

    }

//***************************************************************Lock Button / Lock Pane*************************************************************

    /**
     * Lock pane is a view that has the height and width of the screen.
     * If it is visible it will prevent the user from interacting with anything beneath it.
     * Lock button must be created after lock pane to ensue the lock pane can be removed.
     */
    private void createLockPane() {
        final WindowManager.LayoutParams params3 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        windowManager.addView(parent, params3);
        parent.addView(lockPane, params3);
        lockPane.setVisibility(View.GONE);
        parent.setVisibility(View.GONE);
    }

    /**
     * Creates the lockButton. LockButton is an interface to control other views
     */
    private void createLockButton() {
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER;
        params.x = AppDelegate.getLockButtonXcord();
        params.y = AppDelegate.getLockButtonYcord();
        //  params.windowAnimations = android.R.style.Animation_Translucent;
        windowManager.addView(lockButton, params);
        try {
            lockButton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!moving) {
                        if (panelIsOpen) {
                            lockButton.openPanelAnimation(true);
                            panelIsOpen = false;

                        } else {
                            panelIsOpen = true;
                            lockButton.openPanelAnimation(false);

                        }
                    }
                    return false;
                }
            });
            lockButton.setOnTouchListener(new View.OnTouchListener() {
                private WindowManager.LayoutParams paramsF = params;
                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            initialX = paramsF.x;
                            initialY = paramsF.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            lockButton.setHardShadow();
                            break;
                        case MotionEvent.ACTION_UP:
                            AppDelegate.setLockButtonCords(paramsF.x, paramsF.y);
                            moving = false;//this stops the long Click listener firing when moving the button
                            lockButton.setSoftShadow();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            moving = true;
                            paramsF.x = initialX + (int) (event.getRawX() - initialTouchX);
                            paramsF.y = initialY + (int) (event.getRawY() - initialTouchY);
                            windowManager.updateViewLayout(lockButton, paramsF);
                            break;
                    }
                    return false;
                }


            });
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /**
     * Lock button can be expanded to show 3 other buttons. Here is the interface for them
     */
    public void lockButtonPanelListener() {
        lockButton.addListener(new LockPanelListener() {
            @Override
            public void onButtonOnePressed(boolean isPressed) {
                if (isPressed) {
                    lockPane.setVisibility(View.VISIBLE);
                    lockPane.setBackgroundColor(Color.GREEN);
                    startAnimation(lockPane, 500, true);
                    lockButton.setLockLogo(true);
                } else {
                    lockPane.setBackgroundColor(Color.RED);
                    lockPane.setVisibility(View.VISIBLE);
                    parent.setVisibility(View.VISIBLE);
                    startAnimation(lockPane, 1000, false);
                    lockButton.setLockLogo(false);

                }

            }

            @Override
            public void onButtonTwoPressed(boolean isPressed) {
                if (isPressed) {

                    createLockAdjustableView();

                } else {

                    setDummyAdjustableView();
                }
            }

            @Override
            public void onButtonThreePressed(boolean isPressed) {
                killAdjustableView();
                killLockButton();

            }
        });
    }
//***************************************************************Adjustable View *************************************************************
    /**
     * Creates the adjustableView
     * Note it kills the lock button then recreates it on top after.
     */
    public void createLockAdjustableView() {
        killView(topView);
        killView(leftView);
        killView(bottomView);
        killView(rightView);
        adjustableView.setVisibility(View.VISIBLE);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        final WindowManager.LayoutParams params1 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params1.x = AppDelegate.getLockButtonXcord();
        params1.y = AppDelegate.getLockButtonYcord();
        killLockButton();
        windowManager.addView(adjustableView, params);
        windowManager.addView(lockButton, params1);

    }
    /**
     * Replicates the last cordinates of the adjustable lock view to make a lockPane with
     * an unlocked section
     */
    private void createReplacementRestrictedView() {

        topView = new ImageView(this);
        leftView = new ImageView(this);
        bottomView = new ImageView(this);
        rightView = new ImageView(this);
        createWindowViewParams(topView, AppDelegate.getScreenWidth(), AppDelegate.getTopHeight(), Gravity.TOP);
        createWindowViewParams(leftView, AppDelegate.getleftWidth(), AppDelegate.getScreenHeight(), Gravity.LEFT);
        createWindowViewParams(bottomView, AppDelegate.getScreenWidth(), AppDelegate.getBottomHeight(), Gravity.BOTTOM);
        createWindowViewParams(rightView, AppDelegate.getRightWidth(), AppDelegate.getScreenHeight(), Gravity.RIGHT);

    }

    private void createWindowViewParams(ImageView view, int width, int height, int gravity) {
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width,
                height,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = gravity;
        windowManager.addView(view, params);
        view.setBackgroundColor(getResources().getColor(R.color.material_light_blue_faded));
    }
    /**
     * The adjustable view sits on top of everything and it's transparent center prevents
     * clicks passing through. This dummy view is the solution
     */
    public void setDummyAdjustableView() {
        killAdjustableView();
        killLockButton();
        createReplacementRestrictedView();
        createLockButton();
    }
    //***************************************************************Animation*************************************************************
    /**
     * Animate for fading a view
     */
    public void startAnimation(View v, int duration, final boolean exitAnimation) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(v, "alpha", 1f, .0f);
        fadeOut.setDuration(duration);
        //  ObjectAnimator fadeIn = ObjectAnimator.ofFloat(v, "alpha", .3f, 1f);
        // fadeIn.setDuration(2000);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //  mAnimationSet.start();
                //    animationHasStarted = false;
                //TODO rethink this. Not very usable.
                if (exitAnimation) {
                    lockPane.setVisibility(View.GONE);
                    parent.setVisibility(View.GONE);

                }


            }
        });
        mAnimationSet.start();
    }
    //***************************************************************View Removal*************************************************************
    private void killView(ImageView view) {
        if (view != null && view.getWindowToken() != null) {
            windowManager.removeView(view);

        }

    }

    private void killLockButton() {
        if (lockButton != null && lockButton.getWindowToken() != null) {
            windowManager.removeView(lockButton);
        }

    }

    private void killAdjustableView() {
        if (adjustableView != null && adjustableView.getWindowToken() != null) {
            windowManager.removeView(adjustableView);
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // try{
        if (parent != null && parent.getWindowToken() != null) windowManager.removeView(parent);
        if (lockButton != null && lockButton.getWindowToken() != null)
            windowManager.removeView(lockButton);
        // }catch ()

    }

    //***************************************************************Unused*************************************************************
    /**
     * Coudlnt get this working. Purpose is to disable all soft buttons.
     */
    private void setImmersiveFullScreenMode() {

        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.

     /*   decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);

*/
    }


}
