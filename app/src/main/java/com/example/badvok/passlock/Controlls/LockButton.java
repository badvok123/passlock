package com.example.badvok.passlock.Controlls;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.animation.ValueAnimator;

import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.badvok.passlock.Animations.AnimateWidth;
import com.example.badvok.passlock.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by badvok on 27-Sep-15.
 */
public class LockButton extends FrameLayout {

    ImageView button, shadow, logo, expander, edge;
    ImageView button1, button2, button3;
    int mainButtonSize, logoSize, buttonSize, padding;
    boolean pressed1 = true, pressed3 = false, pressed2 = false, adjustableViewSet = false;
    private List<LockPanelListener> listeners;

    public LockButton(Context context) {
        super(context);
        init(context);
    }

    public LockButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LockButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public LockButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context ctx) {
        View view = inflate(ctx, R.layout.lock_button, null);
        addView(view);
        listeners = new ArrayList<LockPanelListener>();

        addLayoutViews();
        Resources r = getResources();
        mainButtonSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, r.getDisplayMetrics());
        logoSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());
        buttonSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());
        padding =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, r.getDisplayMetrics());

        setViewParams();
        setViewColours(ctx);


        defaultButtonIcons();
        shadow.setAlpha(0.5f);

        setOnClickListeners();
        button1.setClickable(false);
        button2.setClickable(false);
        button3.setClickable(false);
    }

    private void addLayoutViews(){
        button = (ImageView) findViewById(R.id.buttonImg);
        shadow = (ImageView) findViewById(R.id.shadowImg);
        expander = (ImageView) findViewById(R.id.expander);
        edge = (ImageView) findViewById(R.id.edge);
        logo = (ImageView) findViewById(R.id.logo);
        button1 = (ImageView) findViewById(R.id.button1);
        button2 = (ImageView) findViewById(R.id.button2);
        button3 = (ImageView) findViewById(R.id.button3);
    }
    /**
     * setViewParams called once to set all the view sizes.
     *
     * Animations will alter expander width
     */

    //***************************************************************View sizes*************************************************************

    private void setViewParams(){
        adjustFrameViewParams(button, mainButtonSize, mainButtonSize, Gravity.CENTER, 0, padding, 0, padding);
        adjustFrameViewParams(shadow, mainButtonSize, mainButtonSize, Gravity.CENTER, 0, padding, 0, 0);
        adjustFrameViewParams(logo, logoSize, logoSize, Gravity.CENTER, 0, 0, 0, 0);
        adjustFrameViewParams(button1, buttonSize, buttonSize, Gravity.CENTER, 0, 0, 0, 0);
        adjustFrameViewParams(button2, buttonSize, buttonSize, Gravity.CENTER, 0, 0, 0, 0);
        adjustFrameViewParams(button3, buttonSize, buttonSize, Gravity.CENTER, 0, 0, 0, 0);
        adjustViewParams(edge, mainButtonSize - padding, mainButtonSize - padding, Gravity.CENTER, -mainButtonSize / 2, 0, 0, 0);
        adjustViewParams(expander, 2, mainButtonSize - padding, Gravity.CENTER, (mainButtonSize + padding + padding + padding) / 2, 0, 0, 0);
    }

    public void adjustViewParams(View v, int width, int height, int gravity, int left, int top, int right, int bottom) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height, gravity);
        params.setMargins(left, top, right, bottom);
        v.setLayoutParams(params);
    }

    public void adjustFrameViewParams(View v, int width, int height, int gravity, int left, int top, int right, int bottom) {
        LayoutParams params = new LayoutParams(width, height, gravity);
        params.setMargins(left, top, right, bottom);
        v.setLayoutParams(params);
    }

    //***************************************************************Colour and icons*************************************************************

    /**
     * setView colours to material greatness
     */
    private void setViewColours(Context ctx){
        overlayColour(button, getResources().getColor(R.color.material_deep_teal_200), ctx);
        overlayColour(shadow, getResources().getColor(R.color.material_blue_grey_950), ctx);
        overlayColour(edge, getResources().getColor(R.color.accent_material_light), ctx);
        overlayColour(button1, getResources().getColor(R.color.switch_thumb_material_light), ctx);
        overlayColour(button2, getResources().getColor(R.color.switch_thumb_material_light), ctx);
        overlayColour(button3, getResources().getColor(R.color.switch_thumb_material_light), ctx);
        expander.setBackgroundColor(getResources().getColor(R.color.accent_material_light));
    }

    private void overlayColour(View v, int colour, Context ctx) {
        int color = colour;
        //      color = getResources().getColor(R.color.material_deep_teal_200);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY);
        Drawable drawable = ctx.getResources().getDrawable(R.drawable.circle);
        drawable = drawable.mutate();
        drawable.setColorFilter(colorFilter);
        v.setBackground(drawable);
    }

    public void defaultButtonIcons(){
        logo.setImageResource(R.mipmap.ic_lock_open_black_48dp);
        button1.setImageResource(R.mipmap.ic_lock_open_black_48dp);
        button2.setImageResource(R.mipmap.ic_crop_free_black_48dp);
        button3.setImageResource(R.mipmap.ic_close_black_48dp);
    }

    public void setLockLogo(boolean locked) {
        if (locked) {
            logo.setImageResource(R.mipmap.ic_lock_open_black_48dp);
        } else {
            logo.setImageResource(R.mipmap.ic_lock_black_48dp);
        }

    }

    public void setPressedAndNotPressedLogosForButton(ImageView button, boolean pressedState, Drawable pressed, Drawable notPressed){

        if(pressedState){
            button.setBackground(pressed);
        }else{
            button.setBackground(notPressed);
        }

    }

    public void setHardShadow() {
        shadow.setAlpha(0.7f);
    }

    public void setSoftShadow() {
        shadow.setAlpha(0.5f);
    }

    //***************************************************************Animations*************************************************************

    public void openPanelAnimation(boolean open) {
        if (!open) {

            button1.setClickable(true);
            button2.setClickable(true);
            button3.setClickable(true);
            AnimateWidth anim = new AnimateWidth(expander, mainButtonSize*3);

            anim.setDuration(500);

            expander.startAnimation(anim);

            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    animateMargin(button3, mainButtonSize*3, 200);
                    animateMargin(button2, mainButtonSize*2, 500);
                    animateMargin(button1, mainButtonSize+padding, 800);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {

            button1.setClickable(false);
            button2.setClickable(false);
            button3.setClickable(false);
            //  animateMargin(button3,0,0);
            //  animateMargin(button2,0,300);
            animateMarginReduction(button1, mainButtonSize, 0);
            animateMarginReduction(button2, mainButtonSize*2, 200);
            animateMarginReduction(button3, mainButtonSize*3, 500);

            //this.leftFragmentWidthPx = leftFragmentWidthPx;
            AnimateWidth anim = new AnimateWidth(expander, 4);
            anim.setStartOffset(800);
            anim.setDuration(500);
            button.startAnimation(anim);
        }

    }

    public void animateMargin(final View view, final int margin, int offSet) {

        Animation a = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                LayoutParams params = new LayoutParams(buttonSize, buttonSize, Gravity.NO_GRAVITY);
                params.leftMargin = (int) (margin * interpolatedTime);
                //params.rightMargin = 0;
                view.setLayoutParams(params);
            }
        };
        a.setStartOffset(offSet);
        a.setDuration(500); // in ms
        view.startAnimation(a);
    }

    public void animateMarginReduction(final View view, final int margin, int offSet) {

        //   final View animatedView = view.findViewById(R.id.animatedView);
        final LayoutParams params = new LayoutParams(buttonSize-padding, buttonSize-padding, Gravity.NO_GRAVITY);

        ValueAnimator animator = ValueAnimator.ofInt(margin, 0);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.leftMargin = (Integer) valueAnimator.getAnimatedValue();
                view.setLayoutParams(params);
            }
        });
        animator.setStartDelay(offSet);
        animator.setDuration(500);
        animator.start();
    }

    //***************************************************************Interface*************************************************************

    private void setOnClickListeners() {
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pressed1) {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonOnePressed(false);
                    }
                    button1.setImageResource(R.mipmap.ic_lock_open_black_48dp);
                    pressed1 = false;
                } else {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonOnePressed(true);
                    }
                    button1.setImageResource(R.mipmap.ic_lock_black_48dp);//TODO improve these logo's and how/when they are set
                    pressed1 = true;
                }

            }
        });
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pressed2) {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonTwoPressed(false);
                    }
                    button2.setImageResource(R.mipmap.ic_crop_black_48dp);

                    pressed2 = false;
                } else {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonTwoPressed(true);
                    }
                    button2.setImageResource(R.mipmap.ic_crop_din_black_48dp);
                    pressed2 = true;
                }
            }
        });
        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pressed3) {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonThreePressed(false);
                    }

                    pressed3 = false;
                } else {
                    for (LockPanelListener listener : listeners) {
                        listener.onButtonThreePressed(true);
                    }
                    pressed3 = true;
                }
            }
        });
    }



    public void addListener(LockPanelListener listener) {
        if (listener != null)
            listeners.add(listener);
    }
}
